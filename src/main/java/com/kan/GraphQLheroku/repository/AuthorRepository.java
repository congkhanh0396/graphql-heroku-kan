package com.kan.GraphQLheroku.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kan.GraphQLheroku.model.Author;
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
}
