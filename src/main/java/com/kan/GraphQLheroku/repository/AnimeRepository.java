package com.kan.GraphQLheroku.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kan.GraphQLheroku.model.Anime;
@Repository
public interface AnimeRepository extends JpaRepository<Anime, Long> {

}
