package com.kan.GraphQLheroku.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.kan.GraphQLheroku.model.Author;
import com.kan.GraphQLheroku.repository.AnimeRepository;
import com.kan.GraphQLheroku.repository.AuthorRepository;





@Component
public class Mutation implements GraphQLMutationResolver {
	private AuthorRepository authorRepository;
	private AnimeRepository animeRepository;

	@Autowired
	public Mutation(AuthorRepository authorRepository, AnimeRepository animeRepository) {
		this.authorRepository = authorRepository;
		this.animeRepository = animeRepository;
	}

	public Author createAuthor(String name, Long age) {
		Author author = new Author();
		author.setName(name);
		author.setAge(age);
		
		authorRepository.save(author);
		
		return author;
	}
	
	
}
