package com.kan.GraphQLheroku.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.kan.GraphQLheroku.model.Author;
import com.kan.GraphQLheroku.repository.AnimeRepository;
import com.kan.GraphQLheroku.repository.AuthorRepository;





@Component
public class Query implements GraphQLQueryResolver {
	private AuthorRepository authorRepository;
	private AnimeRepository animeRepository; 
	
	@Autowired
	public Query(AuthorRepository authorRepository, AnimeRepository animeRepository ) {
		this.authorRepository = authorRepository;
		this.animeRepository = animeRepository; 
	}
	
	public Iterable<Author> findAllAuthors(){
		return authorRepository.findAll();
	}
}
