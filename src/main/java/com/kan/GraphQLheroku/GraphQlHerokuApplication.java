package com.kan.GraphQLheroku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphQlHerokuApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphQlHerokuApplication.class, args);
	}

}
